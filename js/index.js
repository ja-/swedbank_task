const spollerArray = document.querySelectorAll("[data-spollers]");

let changeActiveSpoller;

if (spollerArray.length > 0) {
	const spollersRegular = [...spollerArray].filter(
		item => !item.dataset.spollers.split(",")[0]
	);

	if (spollersRegular.length > 0) {
		initSpollers(spollersRegular);
	}

	const spollersMedia = [...spollerArray].filter(
		item => item.dataset.spollers.split(",")[0]
	);

	if (spollersMedia.length > 0) {
		const breakpointsArray = [];

		spollersMedia.forEach(item => {
			const params = item.dataset.spollers;
			const breakpoint = {};
			const paramsArray = params.split("");

			breakpoint.value = paramsArray[0];
			breakpoint.type = paramsArray[1] ? paramsArray[1].trim() : "max";
			breakpoint.item = item;

			breakpointsArray.push(breakpoint);
		});

		let mediaQueries = breakpointsArray.map(item => {
			return (
				"(" +
				item.type +
				"-width:" +
				item.value +
				"px)," +
				item.value +
				"," +
				item.type
			);
		});

		mediaQueries = mediaQueries.filter((item, index, self) => {
			return self.indexOf(item) === index;
		});

		mediaQueries.forEach(breakpoint => {
			const paramsArray = breakpoint.split(",");
			const mediaBreakpoint = paramsArray[1];
			const mediaType = paramsArray[2];
			const matchMedia = window.matchMedia(paramsArray[0]);

			const spollersArray = breakpointsArray.filter(item => {
				if (item.value === mediaBreakpoint && item.type === mediaType) {
					return true;
				}
			});

			matchMedia.addListener(() => {
				initSpollers(spollersArray, matchMedia);
			});

			initSpollers(spollersArray, matchMedia);
		});
	}

	function initSpollers(spollersArray, matchMedia = false) {
		spollersArray.forEach(spollersBlock => {
			spollersBlock = matchMedia ? spollersBlock.item : spollersBlock;

			if (matchMedia.matches || !matchMedia) {
				spollersBlock.classList.add("_init");

				initSpollerBody(spollersBlock);
			} else {
				spollersBlock.classList.remove("_init");

				initSpollerBody(spollersBlock, false);
			}
		});
	}

	function initSpollerBody(spollersBlock, hideSpollerBody = true) {
		const spollerTitles = spollersBlock.querySelectorAll("[data-spoller]");
		if (spollerTitles.length > 0) {
			spollerTitles.forEach(spollerTitle => {
				if (hideSpollerBody) {
					spollerTitle.removeAttribute("tabindex");

					if (!spollerTitle.classList.contains("_active")) {
						spollerTitle.nextElementSibling.hidden = true;
					}
				} else {
					spollerTitle.setAttribute("tabindex", "-1");
					spollerTitle.nextElementSibling.hidden = false;
				}
			});
		}
	}

	changeActiveSpoller = index => {
		const allSpollers = [...document.querySelectorAll("[data-spoller]")];
		const currentSpoller = allSpollers[index];

		const spollerTitle = currentSpoller.hasAttribute("data-spoller")
			? currentSpoller
			: currentSpoller.closest("[data-spoller]");
		const spollersBlock = spollerTitle.closest("[data-spollers]");
		const oneSpoller = spollersBlock.hasAttribute("data-one-spoller")
			? true
			: false;

		if (!spollersBlock.querySelectorAll("._slide").length) {
			if (oneSpoller && !currentSpoller.classList.contains("_active")) {
				hideSpollersBody(spollersBlock);
			}

			currentSpoller.classList.toggle("_active");

			_slideToggle(currentSpoller.nextElementSibling, 500);
		}
	};

	function hideSpollersBody(spollersBlock) {
		const spollerActiveTitle = spollersBlock.querySelector(
			"[data-spoller]._active"
		);

		if (spollerActiveTitle) {
			spollerActiveTitle.classList.remove("_active");
			_slideUp(spollerActiveTitle.nextElementSibling, 500);
		}
	}
}

const _slideUp = (target, duration = 500) => {
	if (!target.classList.contains("_slide")) {
		target.classList.add("_slide");

		target.style.transitionProperty = "height, margin, padding";

		target.style.transitionDuration = duration + "ms";

		target.style.height = target.offsetHeight + "px";

		target.offsetHeight;

		target.style.overflow = "hidden";

		target.style.height = 0;

		target.style.paddingTop = 0;

		target.style.paddingBotton = 0;

		target.style.marginTop = 0;

		target.style.marginBottom = 0;

		window.setTimeout(() => {
			target.hidden = true;

			target.style.removeProperty("height");

			target.style.removeProperty("padding-top");
			target.style.removeProperty("padding-bottom");
			target.style.removeProperty("margin-top");
			target.style.removeProperty("margin-bottom");

			target.style.removeProperty("overflow");

			target.style.removeProperty("transition duration");
			target.style.removeProperty("transition-property");

			target.classList.remove("_slide");
		}, duration);
	}
};

const _slideDown = (target, duration = 500) => {
	if (!target.classList.contains("_slide")) {
		target.classList.add("_slide");

		if (target.hidden) {
			target.hidden = false;
		}

		let height = target.offsetHeight;

		target.style.overflow = "hidden";

		target.style.height = 0;

		target.style.paddingTop = 0;

		target.style.paddingBotton = 0;

		target.style.marginTop = 0;

		target.style.marginBottom = 0;

		target.offsetHeight;

		target.style.transitionProperty = "height, margin, padding";
		target.style.transitionDuration = duration + "ms";

		target.style.height = height + "px";

		target.style.removeProperty("padding-top");

		target.style.removeProperty("padding-bottom");
		target.style.removeProperty("margin-top");

		target.style.removeProperty("margin-bottom");
		window.setTimeout(() => {
			target.style.removeProperty("height");

			target.style.removeProperty("averflow");
			target.style.removeProperty("transition-duration");

			target.style.removeProperty("transition-property");

			target.classList.remove("_slide");
		}, duration);
	}
};

const _slideToggle = (target, duration = 500) => {
	if (target.hidden) {
		return _slideDown(target, duration);
	} else {
		return _slideUp(target, duration);
	}
};

const checkValid = (formElem, tagName) => {
	if (tagName === "INPUT" && formElem.type !== "radio") {
		const value = formElem.value.trim();

		formElem.addEventListener("input", () => {
			formElem.classList.remove("_no-valid");
		});

		if (value !== "") {
			formElem.removeEventListener("input", () => {
				formElem.classList.remove("_no-valid");
			});

			return true;
		} else {
			return false;
		}
	}

	if (tagName === "TEXTAREA") {
		const value = formElem.value.trim();

		if (value !== "") {
			return true;
		} else {
			return false;
		}
	}

	if (tagName === "INPUT" && formElem.type === "radio") {
		const checkedRadio = document.querySelector(
			'.form__elem[name="gender"]:checked'
		);

		if (checkedRadio) {
			return true;
		} else {
			return false;
		}
	}

	if (tagName === "INPUT" && formElem.type === "checkbox") {
		const checkedRadio = document.querySelector(
			'.form__elem[name="policy-privacy"]:checked'
		);

		if (checkedRadio) {
			return true;
		} else {
			return false;
		}
	}

	if (tagName === "SELECT") {
		const selectValue = formElem.options[formElem.options.selectedIndex];
		const value = selectValue.value.trim();

		if (value !== "") {
			return true;
		} else {
			return false;
		}
	}
};

const submitBtn = document.querySelector(".form__submit");

submitBtn.addEventListener("click", () => {
	const textarea = document.querySelector(".text-area.form__elem");
	const checkBox = document.querySelector(
		".question-item__policy-privacy-checkbox.form__elem"
	);

	if (!textarea.value.trim()) {
		textarea.classList.add("_no-valid");
	} else {
		textarea.classList.remove("_no-valid");
	}

	if (!checkBox.checked) {
		checkBox.classList.add("_no-valid");
	} else {
		checkBox.classList.remove("_no-valid");
	}

	textarea.addEventListener("input", e => {
		if (!textarea.value.trim()) {
			textarea.classList.add("_no-valid");
		} else {
			textarea.classList.remove("_no-valid");
		}

		if (textarea.value.trim() && checkBox.checked) {
			submitBtn.disabled = false;
		} else {
			submitBtn.disabled = true;
		}
	});

	checkBox.addEventListener("change", () => {
		if (checkBox.checked) {
			checkBox.classList.remove("_no-valid");
		} else {
			checkBox.classList.add("_no-valid");
		}

		if (checkBox.checked && textarea.value.trim()) {
			submitBtn.disabled = false;
		} else {
			submitBtn.disabled = true;
		}
	});

	if (checkValid(textarea, textarea.tagName) && checkBox.checked) {
	} else {
		submitBtn.disabled = true;
	}
});

function checkAllFieldsValid() {
	const allFieldsWithError = document.querySelectorAll(".form__elem._no-valid");

	console.log(allFieldsWithError);
	if (allFieldsWithError.length > 0) {
		return false;
	} else {
		return true;
	}
}

const modals = document.querySelector(".modal");
const closeModalButton = document.querySelector(".modal__close");
const form = document.querySelector("#loan-application");

const openModal = () => {
	if (checkAllFieldsValid()) {
		modals.classList.add("_active");
		document.body.classList.add("_stop-scroll");
	}
};

const closeModal = () => {
	modals.classList.remove("_active");
	document.body.classList.remove("_stop-scroll");
};

document.body.addEventListener("click", e => {
	const target = e.target;

	if (!target.closest(".modal__content")) {
		closeModal();
	}
});

const sendForm = e => {
	e.preventDefault();

	const formData = new FormData(form);
	const summaryTable = document.querySelector(".summary-table");
	const data = {
		name: formData.get("name"),
		age: formData.get("age"),
		loanPeriod: formData.get("loan-period"),
		gender: formData.get("gender"),
		needLoan: formData.get("need-loan"),
		policyPrivacy: formData.get("policy-privacy"),
	};

	for (const [key, value] of formData) {
		console.log(`${key}: ${value}\n`);
	}
	summaryTable.textContent = "";
	summaryTable.insertAdjacentHTML(
		"afterbegin",
		`
			<tbody>
				<tr>
					<td>Name:</td>
					<td>${data.name}</td>
				</tr>
				<tr>
					<td>Age:</td>
					<td>${data.age}</td>
				</tr>
				<tr>
					<td>Loan period</td>
					<td>${data.loanPeriod}</td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td>${data.gender}</td>
				</tr>
				<tr>
					<td>Need a loan</td>
					<td>${data.needLoan}</td>
				</tr>
				<tr>
					<td>Policy privacy</td>
					<td>${data.policyPrivacy}</td>
				</tr>
			</tbody>
		`
	);

	changeActiveSpoller(0);
	form.reset();

	openModal();
};

form.addEventListener("submit", sendForm);
closeModalButton.addEventListener("click", closeModal);

const allControlButton = document.querySelectorAll(".control-items__button");

const handlerControlButtons = e => {
	const typeBtn = e.target.dataset.controlTypeBtn;
	const indexBtn = e.target.dataset.controlBtn;

	if (typeBtn === "next") {
		nextQuestion(indexBtn, e.target);
	}
	if (typeBtn === "prev") {
		prevQuestion(indexBtn);
	}
};

allControlButton.forEach(item =>
	item.addEventListener("click", handlerControlButtons)
);

const nextQuestion = (id, elem) => {
	const parentBlock = elem.closest(".block__text");
	const formElem = parentBlock.querySelector(".form__elem");
	const tagName = formElem.tagName;

	if (checkValid(formElem, tagName)) {
		changeActiveSpoller(++id);
	} else {
		formElem.classList.add("_no-valid");
	}
};

const prevQuestion = id => {
	changeActiveSpoller(--id);
};
